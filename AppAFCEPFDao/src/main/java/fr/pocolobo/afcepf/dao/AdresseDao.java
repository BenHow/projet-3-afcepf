package fr.pocolobo.afcepf.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.AdresseIDao;
import fr.pocolobo.afcepf.entity.Adresse;

@Remote(AdresseIDao.class)
@Stateless
public class AdresseDao extends GenericDao<Adresse> implements AdresseIDao {

}
