package fr.pocolobo.afcepf.controller;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;

import fr.pocolobo.afcepf.business.api.CoordonneesIBusiness;
import fr.pocolobo.afcepf.entity.Coordonnees;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;


@ManagedBean(name = "mbImport")
@ViewScoped
public class FileImportBean2 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Part fichier; 
	@ManagedProperty(value = "#{mbConnect.utilisateur}")
	private Utilisateur utilisateur;
	
	@ManagedProperty(value = "#{mbEditionUtilisateur.utilisateurEdite}")
	private Utilisateur utilisateurEdite;
	
	@EJB
	private CoordonneesIBusiness ibuCoord;
	



	public void sauvegarder() {

		
		try (InputStream input = fichier.getInputStream()) {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String destination = servletContext.getRealPath("images");
			String nomFichier = fichier.getSubmittedFileName();
			Files.copy(input, new File(destination, nomFichier).toPath());
			utilisateur.getCoordonnees().setPhoto("images/"+nomFichier);
			Coordonnees nouvelles = ibuCoord.modifier(utilisateur.getCoordonnees());
			utilisateur.setCoordonnees(nouvelles);
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sauvegarderUtilisateurEdite() {

		
		try (InputStream input = fichier.getInputStream()) {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String destination = servletContext.getRealPath("images");
			String nomFichier = fichier.getSubmittedFileName();
			Files.copy(input, new File(destination, nomFichier).toPath());
			utilisateurEdite.getCoordonnees().setPhoto("images/"+nomFichier);
			Coordonnees nouvelles = ibuCoord.modifier(utilisateurEdite.getCoordonnees());
			utilisateurEdite.setCoordonnees(nouvelles);
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}


	public Part getFichier() {
		return fichier;
	}


	public void setFichier(Part fichier) {
		this.fichier = fichier;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}


	public CoordonneesIBusiness getIbuCoord() {
		return ibuCoord;
	}


	public void setIbuCoord(CoordonneesIBusiness ibuCoord) {
		this.ibuCoord = ibuCoord;
	}
	
	public Utilisateur getUtilisateurEdite() {
		return utilisateurEdite;
	}


	public void setUtilisateurEdite(Utilisateur utilisateurEdite) {
		this.utilisateurEdite = utilisateurEdite;
	}


}
