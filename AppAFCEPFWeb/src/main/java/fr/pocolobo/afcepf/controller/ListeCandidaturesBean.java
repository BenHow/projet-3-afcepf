package fr.pocolobo.afcepf.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.DossierCandidatureIBusiness;
import fr.pocolobo.afcepf.entity.DossierCandidature;
import fr.pocolobo.afcepf.entity.DossierCandidature.Avancement;

@ManagedBean
@ViewScoped
public class ListeCandidaturesBean {

	private List<DossierCandidature> listeDossiers;

	@EJB
	private DossierCandidatureIBusiness dcBu;

	@ManagedProperty(value = "#{mbConnect}")
	private ConnexionBean mbConnect;

	@PostConstruct
	public void init() {
		listeDossiers = getAllSelonProfil();
	}
	
	public List<DossierCandidature> getAllSelonProfil() {
		
		if (! mbConnect.isConnecte())
			return new ArrayList<DossierCandidature>();
		
		if (mbConnect.isAdministration())
			return dcBu.getAll();
		else
			return dcBu.rechercherParAttributEntier("stagiaire.id", mbConnect.getUtilisateur().getId());
	}

	public String getAvancement(DossierCandidature dc) {

		if (dc.isRefuse())											return "Refusé"; 
		if (dc.isAccepte())											return "Accepté"; 
		if (dc.getAvancement() == Avancement.initial)				return "Initial"; 
		if (dc.getAvancement() == Avancement.attenteAffectation)	return "Attente affectation"; 
		if (dc.getAvancement() == Avancement.attenteEI)				return "Attente EI"; 
		if (dc.getAvancement() == Avancement.attenteRI)				return "Attente RI"; 
		
		return "";
	}
	
	// =====================================================
	// Restriction d'accès

	public String checkAccess() {
		
		if (!mbConnect.isConnecte()) {
			return NavigationBean.forbiddenUrl;
		}

		return null;
	}

	// =====================================================
	// Recherche

	private String champs;

	public String getChamps() {
		return champs;
	}

	public void setChamps(String champs) {
		this.champs = champs;
	}

	public void rechercher() {
		
		if (champs == null) {
			setListeDossiers(getAllSelonProfil());
			return;
		}

		listeDossiers = dcBu.rechercherParAttributContenu("stagiaire.coordonnees.nom stagiaire.coordonnees.prenom", champs);
	}

	// =====================================================
	// Getters/setters

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

	public List<DossierCandidature> getListeDossiers() {
		return listeDossiers;
	}

	public void setListeDossiers(List<DossierCandidature> listeDossiers) {
		this.listeDossiers = listeDossiers;
	}

	
}