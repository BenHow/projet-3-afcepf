package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;

import fr.pocolobo.afcepf.business.api.GenericIBusiness;
import fr.pocolobo.afcepf.dao.api.AdresseIDao;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.entity.Adresse;

public class AdresseBusiness extends GenericBusiness<Adresse> implements GenericIBusiness<Adresse> {

	@EJB
	private AdresseIDao dao;
	
	@Override
	public GenericIDao<Adresse> getDao() {
		// TODO Auto-generated method stub
		return dao;
	}

}
