package fr.pocolobo.afcepf.business.api;

import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;

public interface PromotionStagiaireIBusiness extends GenericIBusiness<PromotionStagiaire> {
}
