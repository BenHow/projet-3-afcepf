package fr.pocolobo.afcepf.business.api;

import java.util.List;

import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

public interface CoursIBusiness extends GenericIBusiness<Cours> {
	
	List<Cours> getAllPourPromo(int idPromo);

	List<Intervenant> getAllIntervenantsAffectables(Cours c);
}
