package fr.pocolobo.afcepf.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class NavigationBean implements Serializable, ConnexionListener {
	//=====================================================================
	public class MenuItem {

		private String label;

		private String url;
		
		private List<MenuItem> subItems;

		public MenuItem(String label, String url) {
			super();
			this.label = label;
			this.url = url;
			this.subItems = new ArrayList<MenuItem>();
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public List<MenuItem> getSubItems() {
			return subItems;
		}

		public void setSubItems(List<MenuItem> subItems) {
			this.subItems = subItems;
		}
	}

	//=====================================================================
	//	Attributs

	private static final long serialVersionUID = 1L;

	public static String forbiddenUrl = "/forbidden";

	private String websiteTitle = "EQL Portal";

    @ManagedProperty(value="#{mbConnect}")
    private ConnexionBean mbConnect;

	private List<MenuItem> menuItems;

	//=====================================================================
	//	Initialisation

	@PostConstruct
	public void init() {
		
		mbConnect.addListener(this);

		menuItems = new ArrayList<MenuItem>();
		
		MenuItem menuCandidature = new MenuItem("Candidatures", "#");
		
		if (! mbConnect.isConnecte()) {
			menuCandidature.getSubItems().add(new MenuItem("Déposer une candidature", "precandidature"));
		}
		
		if (mbConnect.isStagiaire()) {
			menuCandidature.getSubItems().add(new MenuItem("Nouvelle candidature", "dossiercandidature"));
//			TODO à compléter
			menuCandidature.getSubItems().add(new MenuItem("Suivre une candidature", "listeCandidatures"));
		}
		
		if (mbConnect.isAdministration()) {
			menuCandidature.getSubItems().add(new MenuItem("Candidatures en cours", "listeCandidatures"));			
		}
		
		menuItems.add(menuCandidature);

		if (mbConnect.isAdministration()) {

			MenuItem menuRH = new MenuItem("Espace RH", "#");
			menuRH.getSubItems().add(new MenuItem("Intervenants", "intervenants"));
			menuRH.getSubItems().add(new MenuItem("Stagiaires", "stagiaires"));
			menuItems.add(menuRH);

			MenuItem menuPedago = new MenuItem("Espace pédagogie", "#");
			menuPedago.getSubItems().add(new MenuItem("Cursus", "cursus"));
			menuPedago.getSubItems().add(new MenuItem("Promotions", "promotions"));
			menuItems.add(menuPedago);
		}
	}
	
	@Override
	public void utilisateurChanged() {
		init();
	}
	
	//=====================================================================
	//	Getter/Setters
	
	public List<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public String getWebsiteTitle() {
		return websiteTitle;
	}

	public void setWebsiteTitle(String websiteTitle) {
		this.websiteTitle = websiteTitle;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

}