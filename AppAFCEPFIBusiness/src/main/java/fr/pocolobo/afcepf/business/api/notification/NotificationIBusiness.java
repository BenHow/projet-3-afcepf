package fr.pocolobo.afcepf.business.api.notification;

import java.util.List;

import fr.pocolobo.afcepf.business.api.GenericIBusiness;
import fr.pocolobo.afcepf.entity.notification.Notification;
import fr.pocolobo.afcepf.entity.notification.Notification.Type;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;

public interface NotificationIBusiness extends GenericIBusiness<Notification> {

	void notifierAdministration(String message, Type t, String url);
	
	void creerNotification(Utilisateur u, Type t, String message, String url);
	
	List<Notification> getAllPourUtilisateur(Utilisateur u);
	
}
