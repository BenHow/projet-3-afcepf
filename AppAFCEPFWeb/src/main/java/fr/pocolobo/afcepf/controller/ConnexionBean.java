package fr.pocolobo.afcepf.controller;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.pocolobo.afcepf.business.api.UtilisateurIBusiness;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;
import fr.pocolobo.afcepf.entity.utilisateurs.UtilisateurHelper;

@ManagedBean(name = "mbConnect")
@SessionScoped
public class ConnexionBean {

	public UtilisateurIBusiness getIbuUtilisateur() {
		return ibuUtilisateur;
	}

	public void setIbuUtilisateur(UtilisateurIBusiness ibuUtilisateur) {
		this.ibuUtilisateur = ibuUtilisateur;
	}

	private String login;
	private String password;
	private Utilisateur utilisateur;
	private String message;

	private ArrayList<ConnexionListener> listeners;

	@EJB
	private UtilisateurIBusiness ibuUtilisateur;


	public ConnexionBean() {
		super();

		listeners = new ArrayList<ConnexionListener>();
	}

	public String getNomAffiche() {

		String nomAffiche;

		if (utilisateur == null) {
			nomAffiche = "Non connecté";
		} else {
			nomAffiche = utilisateur.getCoordonnees().getPrenom() + " " + utilisateur.getCoordonnees().getNom();
		}

		return nomAffiche;

	}

	public String getphotoAffichee() {

		String photoAffichee;

		if (utilisateur == null) {
			photoAffichee = "";
		} else {
			photoAffichee = utilisateur.getCoordonnees().getPhoto();
		}
		return photoAffichee;

	}

	public String connect() {

		utilisateur = ibuUtilisateur.connecter(login, password);
		String retour = "";

		if (utilisateur != null) {
			retour = "/index.xhtml?faces-redirect=true";
			message = "";
		} else {
			retour = "/connexion.xhtml?faces-redirect=true";
			message = "Login ou mot de passe incorrect. Veuillez recommencer.";
		}

		changementUtilisateur();

		return retour;
	}

	public String deconnect() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.invalidate();

		changementUtilisateur();

		return "/connexion.xhtml?faces-redirect=true";
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isConnecte() {
		return utilisateur != null;
	}

	public boolean isStagiaire() {
		if (utilisateur == null)
			return false;

		return UtilisateurHelper.isStagiaire(utilisateur);
	}

	public boolean isIntervenant() {
		if (utilisateur == null)
			return false;

		return UtilisateurHelper.isIntervenant(utilisateur);
	}

	public boolean isAdministration() {
		if (utilisateur == null)
			return false;

		return UtilisateurHelper.isAdministration(utilisateur);
	}


	//===========================================================
	// Listeners

	public void addListener(ConnexionListener l) {

		if (! listeners.contains(l))
			listeners.add(l);
	}

	public void changementUtilisateur() {
		for (ConnexionListener l : listeners) {
			l.utilisateurChanged();
		}
	}
}
