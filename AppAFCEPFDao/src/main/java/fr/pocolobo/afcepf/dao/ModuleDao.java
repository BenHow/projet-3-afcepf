package fr.pocolobo.afcepf.dao;

import java.util.List;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.ModuleIDao;
import fr.pocolobo.afcepf.entity.pedago.Module;

@Stateless
public class ModuleDao extends GenericDao<Module> implements ModuleIDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Module> getAllForCursus(int idCursus) {

		return getEntityManager()
				.createQuery("SELECT t FROM Module t WHERE t.cursus.id = " + idCursus + " ORDER BY t.ordre")
				.getResultList();

	}
}
