package fr.pocolobo.afcepf.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.ModuleIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.ModuleIDao;
import fr.pocolobo.afcepf.entity.pedago.Module;

@Remote (ModuleIBusiness.class)
@Stateless
public class ModuleBusiness extends GenericBusiness<Module> implements ModuleIBusiness {

   @EJB
   private ModuleIDao dao;

   @Override
   public GenericIDao<Module> getDao() {
      return dao;
   }

   public void setDao(ModuleIDao dao) {
      this.dao = dao;
   }
   
   public List<Module> getAllForCursus(int idCursus) {
	   return dao.getAllForCursus(idCursus);
   }
}
