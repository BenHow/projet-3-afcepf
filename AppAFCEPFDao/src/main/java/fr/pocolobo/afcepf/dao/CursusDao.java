package fr.pocolobo.afcepf.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.GenericDao;
import fr.pocolobo.afcepf.dao.api.CursusIDao;
import fr.pocolobo.afcepf.entity.pedago.Cursus;

@Remote(CursusIDao.class)
@Stateless
public class CursusDao extends GenericDao<Cursus> implements CursusIDao {

	@Override
	public String getDefaultOrderBy() {
		return "ORDER BY t.nom";
	}
}
