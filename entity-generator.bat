@Echo Off

set /P entity="Entrer un nom d'entite: "

Set "out=.\generated\%entity%"
if not exist "%out%" mkdir %out%

(
  Echo;package fr.pocolobo.afcepf.entity;
  Echo;
  Echo;import java.io.Serializable;
  Echo;
  Echo;import javax.persistence.Column;
  Echo;import javax.persistence.Entity;
  Echo;import javax.persistence.GeneratedValue;
  Echo;import javax.persistence.GenerationType;
  Echo;import javax.persistence.Id;
  Echo;import javax.persistence.Table;
  Echo;
  Echo;@Entity
  Echo;@Table (name="%entity%"^)
  Echo;public class %entity% implements Serializable {
  Echo;
  Echo;   private static final long serialVersionUID = 1L;
  Echo;
  Echo;   @Id
  Echo;   @GeneratedValue(strategy = GenerationType.IDENTITY^)
  Echo;   @Column(name = "id"^)
  Echo;   private int id;
  Echo;
  Echo;   public int getId(^) {
  Echo;      return id;
  Echo;   }
  Echo;
  Echo;   public void setId(int id^) {
  Echo;      this.id = id;
  Echo;   }
  Echo;}
) > "%out%\%entity%.java"

(
  Echo;package fr.pocolobo.afcepf.dao.api;
  Echo;
  Echo;import fr.pocolobo.afcepf.entity.%entity%;
  Echo;
  Echo;public interface %entity%IDao extends GenericIDao^<%entity%^> {
  Echo;}
) > "%out%\%entity%IDao.java"

(
  Echo;package fr.pocolobo.afcepf.dao;
  Echo;
  Echo;import javax.ejb.Stateless;
  Echo;
  Echo;import fr.pocolobo.afcepf.dao.GenericDao;
  Echo;import fr.pocolobo.afcepf.dao.api.%entity%IDao;
  Echo;import fr.pocolobo.afcepf.entity.%entity%;
  Echo;
  Echo;@Stateless
  Echo;public class %entity%Dao extends GenericDao^<%entity%^> implements %entity%IDao {
  Echo;}
) > "%out%\%entity%Dao.java"

(
  Echo;package fr.pocolobo.afcepf.business.api;
  Echo;
  Echo;import fr.pocolobo.afcepf.business.api.GenericIBusiness;
  Echo;import fr.pocolobo.afcepf.entity.%entity%;
  Echo;
  Echo;public interface %entity%IBusiness extends GenericIBusiness^<%entity%^> {
  Echo;}
) > "%out%\%entity%IBusiness.java"

(
  Echo;package fr.pocolobo.afcepf.business;
  Echo;
  Echo;import javax.ejb.EJB;
  Echo;import javax.ejb.Remote;
  Echo;import javax.ejb.Stateful;
  Echo;
  Echo;import fr.pocolobo.afcepf.business.GenericBusiness;
  Echo;import fr.pocolobo.afcepf.business.api.%entity%IBusiness;
  Echo;import fr.pocolobo.afcepf.dao.api.GenericIDao;
  Echo;import fr.pocolobo.afcepf.dao.api.%entity%IDao;
  Echo;import fr.pocolobo.afcepf.entity.%entity%;
  Echo;
  Echo;@Remote (%entity%IBusiness.class^)
  Echo;@Stateful
  Echo;public class %entity%Business extends GenericBusiness^<%entity%^> implements %entity%IBusiness {
  Echo;
  Echo;   @EJB
  Echo;   private %entity%IDao dao;
  Echo;
  Echo;   @Override
  Echo;   public GenericIDao^<%entity%^> getDao(^) {
  Echo;      return dao;
  Echo;   }
  Echo;
  Echo;   public void setDao(%entity%IDao dao^) {
  Echo;      this.dao = dao;
  Echo;   }
  Echo;}
) > "%out%\%entity%Business.java"

echo Les fichiers ont ete generes dans le dossier %out%
set /P entity=""