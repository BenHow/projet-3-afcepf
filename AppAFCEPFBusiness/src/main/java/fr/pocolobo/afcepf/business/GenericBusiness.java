package fr.pocolobo.afcepf.business;

import java.util.List;

import fr.pocolobo.afcepf.business.api.GenericIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;

/**
 * Classe abstraite de base pour la couche business.
 * 
 * Elle fournit des raccourcis vers les méthodes du DAO. Elle devrait être
 * utilisée pour les classes business qui vont travailler avec un type d'entité
 * en particulier.
 * 
 * @author pcker
 *
 * @param <T> l'entitée à gérer
 */
public abstract class GenericBusiness<T> implements GenericIBusiness<T> {

	/**
	 * Les classes dérivées doivent redéfinir cette méthode pour retourner
	 * un dao.
	 * 
	 * @return une instance spécialisée de dao
	 */
	public abstract GenericIDao<T> getDao();
	
	@Override
	public T ajouter(T t) {
		return getDao().ajouter(t);
	}

	@Override
	public boolean supprimer(T t) {
		return getDao().supprimer(t);
	}

	@Override
	public T modifier(T t) {
		return getDao().modifier(t);
	}

	@Override
	public T rechercherParId(Integer i) {
		return getDao().rechercherParId(i);
	}
	
	@Override
	public List<T> getAll() {
		return getDao().getAll();
	}
	
	@Override
	public List<T> rechercherParAttributContenu (String nomAttribut, String valeur) {
		return getDao().rechercherParAttributContenu(nomAttribut, valeur);
	}
	
	@Override
	public List<T> rechercherParAttributEntier (String nomAttribut, int valeur) {
		return getDao().rechercherParAttributEntier(nomAttribut, valeur);
	}

}
