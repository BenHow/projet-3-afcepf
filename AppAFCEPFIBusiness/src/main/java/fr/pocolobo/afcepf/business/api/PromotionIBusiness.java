package fr.pocolobo.afcepf.business.api;

import java.util.Date;
import java.util.List;

import fr.pocolobo.afcepf.entity.pedago.Cursus;
import fr.pocolobo.afcepf.entity.pedago.Promotion;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

public interface PromotionIBusiness extends GenericIBusiness<Promotion> {
	
	public Date getDateDebut(int promoId);
	
	public Date getDateFin(int promoId);
	
	public boolean isPasse(Promotion p);
	
	public boolean isEnCours(Promotion p);
	
	public boolean isFuture(Promotion p);
	
	public List<Stagiaire> getAllStagiaires(int promoId);

	public Promotion creerPromotion(Cursus cursusDeploy, Date dateDeploy, int numeroPromo);

	public List<Promotion> getAllPourCursus(int id);
}
