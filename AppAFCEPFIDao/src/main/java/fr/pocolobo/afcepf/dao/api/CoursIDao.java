package fr.pocolobo.afcepf.dao.api;

import java.util.List;

import fr.pocolobo.afcepf.entity.pedago.Cours;

public interface CoursIDao extends GenericIDao<Cours> {
	
	List<Cours> getAllPourPromo(int idPromo);
}
