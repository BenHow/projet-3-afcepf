package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;

public interface UtilisateurIDao extends GenericIDao<Utilisateur>  {
	
	Boolean exists(String login);
	
	Utilisateur authenticate(String login, String password);
	
	String generateLogin(Utilisateur utilisateur);
	
	String generatePasswordCrypte(Utilisateur utilisateur);
		
	String decryptPassword (String password);
	


}
