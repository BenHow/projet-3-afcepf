package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.CiviliteIBusiness;
import fr.pocolobo.afcepf.dao.api.CiviliteIDao;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.entity.Civilite;

@Remote(CiviliteIBusiness.class)
@Stateless
public class CiviliteBusiness extends GenericBusiness<Civilite> implements CiviliteIBusiness {

	@EJB
	private CiviliteIDao dao;
	
	@Override
	public GenericIDao<Civilite> getDao() {
		// TODO Auto-generated method stub
		return dao;
	}

}
