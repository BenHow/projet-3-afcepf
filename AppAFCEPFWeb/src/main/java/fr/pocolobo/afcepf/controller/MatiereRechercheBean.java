package fr.pocolobo.afcepf.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.pocolobo.afcepf.business.api.GenericIBusiness;
import fr.pocolobo.afcepf.business.api.MatiereIBusiness;
import fr.pocolobo.afcepf.entity.pedago.Matiere;

@ManagedBean
@SessionScoped
public class MatiereRechercheBean extends GenericRechercheBean<Matiere> {
	
	@EJB
	private MatiereIBusiness bu;
	
	private List<Matiere> allMatieres;
	
	private Matiere selection;

	@PostConstruct
	public void init() {
		allMatieres = bu.getAll();
	}
	
	@Override
	protected String getRechercheAttribut() {
		return "nom";
	}
	
	@Override
	protected GenericIBusiness<Matiere> getBusiness() {
		return bu;
	}
	
	public GenericIBusiness<Matiere> getBu() {
		return bu;
	}

	public void setBu(MatiereIBusiness bu) {
		this.bu = bu;
	}


	public List<Matiere> getAllMatieres() {
		return allMatieres;
	}

	public String getAllCSV() {
		String result = "";
		
		for (int i = 0; i < allMatieres.size(); ++i) {

			if (i > 0)
				result += ", ";
			
			result += "\"" + allMatieres.get(i).getNom() + "\"";
		}
		
		return result;
	}

	public void setAllMatieres(List<Matiere> allMatieres) {
		this.allMatieres = allMatieres;
	}

	public Matiere getSelection() {
		return selection;
	}

	public void setSelection(Matiere selection) {
		this.selection = selection;
	}
}
