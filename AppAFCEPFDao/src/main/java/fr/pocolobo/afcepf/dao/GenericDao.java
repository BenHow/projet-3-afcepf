package fr.pocolobo.afcepf.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.pocolobo.afcepf.dao.api.GenericIDao;

public abstract class GenericDao<T> implements GenericIDao<T> {

	@PersistenceContext (unitName = "AppAFCEPFPU")
	private EntityManager em;

	protected EntityManager getEntityManager() {
		return em;
	}
	
	public String getDefaultOrderBy() {
		return null;
	}
	
	@Override
	public T ajouter(T t) {

		em.persist(t);

		return t;
	}

	@Override
	public boolean supprimer(T t) {

		boolean retour;
		
		try {
			t = em.merge(t);
			em.remove(t);
			retour = true;

		}catch (Exception e){
			e.printStackTrace();

			retour = false;
		} 
		return retour;
	}

	@Override
	public T modifier(T t) {

		em.merge(t);

		return t;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T rechercherParId(Integer i) {
		T t = null;
		
		try {

			String className = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
			Class<?> clazz;  
			clazz = Class.forName(className);
			t = (T) em.find(clazz, i);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return t;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() {
		
		List<T> result = null;

		String className = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
		String orderBy = getDefaultOrderBy();
		
		result = em.createQuery("SELECT t FROM " + className + " t " + (orderBy == null ? "" : orderBy)).getResultList();
		
		return result;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> rechercherParAttributContenu(String attribut, String valeur) {
		
		List<T> result = null;

		String className = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
		
		String request = "SELECT t FROM " + className + " t ";
		
		String[] attributes = attribut.split(" ");
		
		if (attributes.length > 0) {

			request += "WHERE t.";
			
			boolean first = true;
			
			for (String att : attributes) {
				
				if (first) {
					first = false;
				} else {
					request += "OR t.";
				}
				
				request += att + " LIKE  \'%" + valeur.toString() + "%' ";
			}
		}
		
		result = em.createQuery(request).getResultList();
		
		return result;
		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<T> rechercherParAttributEntier (String attribut, int valeur) {

		List<T> result = null;

		String className = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
		result = em.createQuery("SELECT t FROM " + className + " t WHERE t." + attribut + " = " + valeur + "").getResultList();
		
		return result;
	}
}
