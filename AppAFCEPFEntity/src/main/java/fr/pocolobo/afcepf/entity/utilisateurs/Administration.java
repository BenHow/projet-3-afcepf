package fr.pocolobo.afcepf.entity.utilisateurs;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("administration")
public class Administration extends Utilisateur  {
	
	private static final long serialVersionUID = 1L;

	private Integer idAdmin;

	@Override
	public ProfileType getProfile() {
		return ProfileType.administrateur;
	}
	
	public Integer getIdAdmin() {
		return idAdmin;
	}

	public void setIdAdmin(Integer idAdmin) {
		this.idAdmin = idAdmin;
	}

	

}
