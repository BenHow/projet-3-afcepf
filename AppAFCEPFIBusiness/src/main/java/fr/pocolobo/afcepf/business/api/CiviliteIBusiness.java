package fr.pocolobo.afcepf.business.api;

import fr.pocolobo.afcepf.entity.Civilite;

public interface CiviliteIBusiness extends GenericIBusiness<Civilite> {

}
