package fr.pocolobo.afcepf.entity.utilisateurs;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.pedago.MatiereIntervenant;

@Entity
@DiscriminatorValue("intervenant")
public class Intervenant extends Utilisateur {
	
	private static final long serialVersionUID = 1L;
	
	private Integer idIntervenant;

	@OneToMany
	private List<MatiereIntervenant> matiereIntervenants;
	
	@OneToMany
	private List<Cours> listeCours;
	
	@Override
	public ProfileType getProfile() {
		return ProfileType.intervenant;
	}
	
	//================================================================
	// Getter/Setters

    public Integer getIdIntervenant() {
		return idIntervenant;
	}

	public void setIdIntervenant(Integer idIntervenant) {
		this.idIntervenant = idIntervenant;
	}

	public List<MatiereIntervenant> getMatiereIntervenants() {
		return matiereIntervenants;
	}

	public void setMatiereIntervenants(List<MatiereIntervenant> matiereIntervenants) {
		this.matiereIntervenants = matiereIntervenants;
	}

	public List<Cours> getListeCours() {
		return listeCours;
	}

	public void setListeCours(List<Cours> listeCours) {
		this.listeCours = listeCours;
	}

}
