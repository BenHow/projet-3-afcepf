package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.pedago.Cursus;

public interface CursusIDao extends GenericIDao<Cursus> {
}
