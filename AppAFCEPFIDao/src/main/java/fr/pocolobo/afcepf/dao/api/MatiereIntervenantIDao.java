package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.pedago.MatiereIntervenant;

public interface MatiereIntervenantIDao extends GenericIDao<MatiereIntervenant> {
}
