package fr.pocolobo.afcepf.controller;

public class Warning {

	public enum WarningType {
		actionCandidature,
		pourvoirPromo		
	};
	
	String data;
	WarningType type;
	boolean resolved = false;
	
	public Warning(WarningType type, String data) {
		this.type = type;
		this.data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + (resolved ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Warning other = (Warning) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (resolved != other.resolved)
			return false;
		if (type != other.type)
			return false;
		return true;
	}	
}