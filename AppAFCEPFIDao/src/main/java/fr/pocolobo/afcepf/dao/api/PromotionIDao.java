package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.pedago.Promotion;

public interface PromotionIDao extends GenericIDao<Promotion> {
}
