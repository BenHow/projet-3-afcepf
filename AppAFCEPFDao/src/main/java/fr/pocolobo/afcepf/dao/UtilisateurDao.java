package fr.pocolobo.afcepf.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.RandomStringUtils;

import com.ibm.icu.text.Transliterator;

import fr.pocolobo.afcepf.dao.api.UtilisateurIDao;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;

@Remote(UtilisateurIDao.class)
@Stateless

public class UtilisateurDao extends GenericDao<Utilisateur> implements UtilisateurIDao {

	@PersistenceContext(unitName = "AppAFCEPFPU")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public Boolean exists(String login) {

		Query query = em.createQuery("SELECT u FROM Utilisateur u WHERE u.login = :paramlogin");
		query.setParameter("paramlogin", login);
		List<Utilisateur> utilisateurs = query.getResultList();

		return utilisateurs.size() > 0 ? true : false;
	}

	@Override
	public Utilisateur authenticate(String login, String password) {
		Utilisateur utilisateur = null;
		try {
			Query query = em.createQuery(
					"SELECT u FROM Utilisateur u WHERE u.login = :paramlogin and u.password =:parampassword");
			query.setParameter("paramlogin", login);
			query.setParameter("parampassword", password);
			utilisateur = (Utilisateur) query.getSingleResult();
		} catch (NoResultException nre) {
			nre.printStackTrace();
		}

		return utilisateur;

	}

	@Override
	public String generateLogin(Utilisateur utilisateur) {

		String login = utilisateur.getCoordonnees().getPrenom().toLowerCase().charAt(0)
				+ utilisateur.getCoordonnees().getNom().toLowerCase();
		Transliterator accentsconverter = Transliterator.getInstance("NFD; [:M:] Remove; NFC; ");
		login = accentsconverter.transliterate(login);

		// ajout du suffixe

		if (exists(login) && ! login.equals(utilisateur.getLogin())) {

			int suffix = 1;
			String loginAvecSuffix = login + suffix;

			while (exists(loginAvecSuffix) && ! loginAvecSuffix.equals(utilisateur.getLogin())) {
				suffix++;
				loginAvecSuffix = login + suffix;

			}
			login = loginAvecSuffix;
		}

		return login;
	}

	@Override
	public String generatePasswordCrypte(Utilisateur utilisateur) {

		String password = RandomStringUtils.randomAlphanumeric(8);
//		return encryptPassword(password);
		return password;
	}

	public String encryptPassword(String password) {
		String crypte = "";
		for (int i = 0; i < password.length(); i++) {
			int c = password.charAt(i) ^ 48;
			crypte = crypte + (char) c;
		}
		return crypte;
	}

	@Override
	public String decryptPassword(String password) {
		String decrypte = "";
		for (int i = 0; i < password.length(); i++) {
			int c = password.charAt(i) ^ 48;
			decrypte = decrypte + (char) c;
		}
		return decrypte;
	}

}
