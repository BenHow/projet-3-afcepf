package fr.pocolobo.afcepf.business.api;

import java.util.List;

import fr.pocolobo.afcepf.entity.DossierCandidature;
import fr.pocolobo.afcepf.entity.pedago.Promotion;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

public interface StagiaireIBusiness extends GenericIBusiness<Stagiaire> {
	
	List<Promotion> getPromotions(int stagiaireId);
	
	Promotion getDernierePromotion(int stagiaireId);
	
	List<DossierCandidature> getDossiersCandidature(int stagiaireId);
	
	List<DossierCandidature> getCandidaturesEnCours(Stagiaire stagiaire);
	
	String getStatutStagiaire(Stagiaire stagiaire);
	
	boolean isCandidatInitial(Stagiaire stagiaire);
	
	boolean isCandidatRefuse(Stagiaire stagiaire);
	
	boolean isEnAttenteRI(Stagiaire stagiaire);
	
	boolean isCandidat(Stagiaire stagiaire);
	
	boolean isEnAttenteEI(Stagiaire stagiaire);
	
	boolean isStagiaireFutur(Stagiaire stagiaire);
	
	boolean isStagiaireActif(Stagiaire stagiaire);
	
	boolean isStagiairePasse(Stagiaire stagiaire);
}
