package fr.pocolobo.afcepf.dao;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.PromotionStagiaireIDao;
import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;

@Stateless
public class PromotionStagiaireDao extends GenericDao<PromotionStagiaire> implements PromotionStagiaireIDao {
}
