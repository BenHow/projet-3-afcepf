package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.Adresse;

public interface AdresseIDao extends GenericIDao<Adresse>{

}
