package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import fr.pocolobo.afcepf.business.api.IntervenantIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.IntervenantIDao;
import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

@Remote (IntervenantIBusiness.class)
@Stateful
public class IntervenantBusiness extends GenericBusiness<Intervenant> implements IntervenantIBusiness {

   @EJB
   private IntervenantIDao dao;

   @Override
   public GenericIDao<Intervenant> getDao() {
      return dao;
   }

   public void setDao(IntervenantIDao dao) {
      this.dao = dao;
   }
}
