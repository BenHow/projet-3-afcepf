package fr.pocolobo.afcepf.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.CiviliteIDao;
import fr.pocolobo.afcepf.entity.Civilite;
@Remote(CiviliteIDao.class)
@Stateless
public class CiviliteDao extends GenericDao<Civilite> implements CiviliteIDao {

}
