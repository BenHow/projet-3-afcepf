package fr.pocolobo.afcepf.dao.notification;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.GenericDao;
import fr.pocolobo.afcepf.dao.api.NotificationIDao;
import fr.pocolobo.afcepf.entity.notification.Notification;

@Stateless
public class NotificationDao extends GenericDao<Notification> implements NotificationIDao {

}
