//package fr.pocolobo.afcepf.business;
//
//import java.util.List;
//
//import javax.ejb.EJB;
//import javax.ejb.Remote;
//import javax.ejb.Stateless;
//
//import fr.pocolobo.afcepf.business.api.ConnexionIBusiness;
//import fr.pocolobo.afcepf.dao.api.UtilisateurIDao;
//import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;
//
//@Remote(ConnexionIBusiness.class)
//@Stateless
////public class ConnexionBusiness implements ConnexionIBusiness {
//	
//    @EJB
//    private UtilisateurIDao proxyUtilisateurDao;
//
//	@Override
//	public Utilisateur creerUtilisateur(Utilisateur utilisateur) {
//		 Utilisateur utilisateurReturned = null;
//	        if(!proxyUtilisateurDao.exists(utilisateur.getLogin())) {
//	            utilisateurReturned = proxyUtilisateurDao.ajouter(utilisateur);
//	        }
//
//	        return utilisateurReturned;
//	}
//
//	@Override
//	public Utilisateur connecter(String login, String password) {
//		 Utilisateur utilisateurConnecte = null;
//		 utilisateurConnecte = proxyUtilisateurDao.authenticate(login, password);
//		
//		return utilisateurConnecte;
//	}
//	
//	@Override
//	public Utilisateur attribuerIdentifiant(Utilisateur utilisateur) {
//		
//		
//		utilisateur.setLogin(proxyUtilisateurDao.generateLogin(utilisateur));
//		utilisateur.setPassword(proxyUtilisateurDao.generatePasswordCrypte(utilisateur));
//		
//		
//		return utilisateur;
//		
//	}
//
//	@Override
//	public String genererLogin(Utilisateur utilisateur) {
//		
//		utilisateur.setLogin(proxyUtilisateurDao.generateLogin(utilisateur));
//		
//		return utilisateur.getLogin();
//	}
//
//	@Override
//	public String genererMdpCrypte(Utilisateur utilisateur) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public String decrypterMdp(String password) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Utilisateur ajouter(Utilisateur t) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public boolean supprimer(Utilisateur t) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public Utilisateur modifier(Utilisateur t) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Utilisateur rechercherParId(Integer i) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Utilisateur> getAll() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Utilisateur> rechercherParAttributContenu(String nomAttribut, String valeur) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Utilisateur> rechercherParAttributEntier(String nomAttribut, int valeur) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//}
